using UnityEngine;
using System.Collections;

public class ChargerAI : Enemy{
    public float cooldownChargeTime = 3;
    public float cooldownChargeTimer = 0;
    public bool isCharging;
    public bool hasShunted;
<<<<<<< HEAD
    public bool died;
    public bool dissolve;

	public AudioSource charge;

    public Vector3 targetPos;
=======
	
	public AudioSource charge;
	
    public Vector3 targetPos;
    GameObject[] floorgos;
>>>>>>> 0b768a5417c445ab7c2feb2b77491c6c2637549c

    public enum AttackState{
        Idle,
        Walk,
        Charge1,
        Charge2,
        Shunt,
        StunnedTrans,
<<<<<<< HEAD
        Stunned,
        Death
=======
        Stunned
>>>>>>> 0b768a5417c445ab7c2feb2b77491c6c2637549c
    }

    public AttackState attackState = AttackState.Walk;

    void Start(){
        InitEntity("Charger", player, 200, 200, 13, 10, 15, 25, 13f, 30, 2);
        cooldownChargeTimer = cooldownChargeTime;
<<<<<<< HEAD
=======
        floorgos = GameObject.FindGameObjectsWithTag("Floor");
>>>>>>> 0b768a5417c445ab7c2feb2b77491c6c2637549c
		
		charge=(AudioSource)gameObject.AddComponent("AudioSource");
		AudioClip myAudioClip;
		myAudioClip =(AudioClip)Resources.Load("Sounds/charger");
		charge.clip = myAudioClip;
    }

	void FixedUpdate(){
        rigidbody.AddForce(new Vector3 (0, -gravity * rigidbody.mass, 0));
<<<<<<< HEAD
        AnimaionManager();
        if(!isDead){
            Attack();
            if(isInDistance || isAggro){
                if(!isAttacking && target){
                    if(cooldownChargeTimer <= 0){
                        isCharging = true;
                        attackState = AttackState.Charge2;
                        if(targetPos == Vector3.zero) targetPos = target.position;
                        Charge();
                        isCharging = false;
                    }else{
                        myTransform.rotation = Quaternion.Slerp(myTransform.rotation, Quaternion.LookRotation(target.position - myTransform.position), rotationSpeed*Time.deltaTime);
                        cooldownChargeTimer -= Time.deltaTime;
                        if(player.GetComponent<PlayerControls>().stunned){
                            StartCoroutine(Stunned());
                        }else{
                            if(!isStunned)
                                attackState = AttackState.Charge1;
                        }
                    }
                }else{
                    if(!isStunned){
                        cooldownChargeTimer = cooldownChargeTime;
                        isCharging = false;
                        if(!hasShunted)StartCoroutine(ShuntPlayer());
=======
        Attack();
        AnimaionManager();
        if(isInDistance || isAggro){
            isAggro = true;
            if(!isAttacking && target){
                if(cooldownChargeTimer <= 0){
                    isCharging = true;
                    attackState = AttackState.Charge2;
                    if(targetPos == Vector3.zero) targetPos = target.position;
                    Charge();
                    isCharging = false;
                }else{
                    myTransform.rotation = Quaternion.Slerp(myTransform.rotation, Quaternion.LookRotation(target.position - myTransform.position), rotationSpeed*Time.deltaTime);
                    cooldownChargeTimer -= Time.deltaTime;
                    if(player.GetComponent<PlayerControls>().stunned){
                        StartCoroutine(Stunned());
                    }else{
                        attackState = AttackState.Charge1;
>>>>>>> 0b768a5417c445ab7c2feb2b77491c6c2637549c
                    }
                }
            }else{
                cooldownChargeTimer = cooldownChargeTime;
<<<<<<< HEAD
=======
                isCharging = false;
                if(!hasShunted)StartCoroutine(ShuntPlayer());
>>>>>>> 0b768a5417c445ab7c2feb2b77491c6c2637549c
            }
        }else{
            if(!died)StartCoroutine(Death());
            if(dissolve){
                animation.Stop();
                float shininess = Mathf.PingPong(Time.time, 1.0F);
                
                GameObject.Find("ChargerModel").renderer.material.SetFloat("_SliceAmount",  Mathf.Sin(Time.time) * 1.0f);
            }
        }
	}

    void AnimaionManager(){
        switch(attackState){
            case AttackState.Idle:
                animation.clip = animation.GetClip("idle");
                animation.CrossFade(animation.clip.name, 0.2f);
                break;
            case AttackState.Walk:
                animation.clip = animation.GetClip("walk");
                animation.CrossFade(animation.clip.name, 0.2f);
                break;
            case AttackState.Charge1:
                animation.clip = animation.GetClip("charge1");
                animation.CrossFade(animation.clip.name, 0.2f);
				charge.Play();
                break;
            case AttackState.Charge2:
                animation.clip = animation.GetClip("charge2");
                animation.CrossFade(animation.clip.name, 0.2f);
				charge.Play ();
                break;
            case AttackState.Shunt:
                animation.clip = animation.GetClip("shunt");
                animation[animation.clip.name].speed = 1.5f;
                animation.CrossFade(animation.clip.name, 0.2f);
                break;
            case AttackState.StunnedTrans:
                animation.clip = animation.GetClip("stunnedTrans");
                animation.CrossFade(animation.clip.name, 0.2f);
                break;
            case AttackState.Stunned:
                animation.clip = animation.GetClip("stunned");
                animation.CrossFade(animation.clip.name, 0.2f);
                break;
<<<<<<< HEAD
            case AttackState.Death:
                animation.clip = animation.GetClip("death");
                animation.Play();
                //animation[animation.clip.name].speed = 2.0f;
                //animation.CrossFade(animation.clip.name, 0.2f);
                break;
=======
>>>>>>> 0b768a5417c445ab7c2feb2b77491c6c2637549c
        }
    }

    void Charge(){
        //transform.position = Vector3.MoveTowards(myTransform.position, myTransform.forward, Time.deltaTime * 20);
        myTransform.position += myTransform.forward * 20 * Time.deltaTime;
    }

    IEnumerator ShuntPlayer(){
        hasShunted = true;
        attackState = AttackState.Shunt;
        yield return new WaitForSeconds(0.3f);
        rigidbody.velocity = Vector3.zero;
        rigidbody.angularVelocity = Vector3.zero;

        Vector3 targetVelocity = transform.forward;
<<<<<<< HEAD
        Entity entity = (Entity)target.GetComponent(typeof(Entity));
	    entity.AdjustHealth(strength);
=======
>>>>>>> 0b768a5417c445ab7c2feb2b77491c6c2637549c
        //targetVelocity = target.TransformDirection(targetVelocity);
        targetVelocity *= 100;
        target.rigidbody.velocity = targetVelocity;

        StartCoroutine(player.GetComponent<PlayerControls>().Stunned());
        yield return new WaitForSeconds(animation.clip.length / 2);
        hasShunted = false;
        targetPos = Vector3.zero;
    }

    IEnumerator Stunned(){
<<<<<<< HEAD
        isStunned = true;
=======
>>>>>>> 0b768a5417c445ab7c2feb2b77491c6c2637549c
        attackState = AttackState.Stunned;
        cooldownChargeTimer = cooldownChargeTime;
        isCharging = false;
        
        yield return new WaitForSeconds(animation.clip.length * 4);
<<<<<<< HEAD
        targetPos = Vector3.zero;
        isStunned = false;
    }

    IEnumerator Death(){
        died = true;
        attackState = AttackState.Death;
        yield return new WaitForSeconds(2);
        DropLoot();
        dissolve = true;
        yield return new WaitForSeconds(2);
        Destroy(gameObject);
=======
        cooldownChargeTimer = cooldownChargeTime;
        targetPos = Vector3.zero;
>>>>>>> 0b768a5417c445ab7c2feb2b77491c6c2637549c
    }

    void OnCollisionEnter(Collision collision){
        if(collision.transform.tag == "Obstacles"){
            if(isAggro){
                //cooldownChargeTimer = cooldownChargeTime;
                StartCoroutine(Stunned());
            }
        }
    }
}

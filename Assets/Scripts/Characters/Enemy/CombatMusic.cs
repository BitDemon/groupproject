using UnityEngine;
using System.Collections;

public class CombatMusic : MonoBehaviour {

	public AudioSource combat;
	public bool isAggro;

	void Start () {
		combat=(AudioSource)gameObject.AddComponent("AudioSource");
		AudioClip myAudio;
		myAudio =(AudioClip)Resources.Load("Sounds/Combat");
		combat.clip=myAudio;
	}
	

		
	
	// Update is called once per frame
	void PleaseWork () {
		if (!GameObject.Find("Enemy").GetComponent<Enemy>().isAggro){
			GameObject.Find("BackgroundMusic").audio.volume=0.1f;
			combat.Play();
	}
}
}
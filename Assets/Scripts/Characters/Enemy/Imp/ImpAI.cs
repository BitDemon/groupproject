using UnityEngine;
using System.Collections;

public class ImpAI : Enemy{
    ImpLeaderAI impLeaderAi;

    public Transform impLeader = null;
    
    public bool attack, died, dissolve;

    public int jumpHeight = 3;
    public int sideJumpDist = 3;

    public enum AttackState{
        Idle,
        Walk,
        Run,
        Attack,
        Spell,
        Death
    }

    public AttackState attackState = AttackState.Walk;

	void Start(){
        InitEntity("Imp", player, 100, 100, 8, 0, 3, 10, 9f, 30, 1);
    }
	
	void FixedUpdate(){
        
        rigidbody.AddForce(new Vector3 (0, -gravity * rigidbody.mass, 0));
        AnimaionManager();

        if(!isDead){
            Attack();
            if(isInDistance || isAggro){
                if(isAttacking){
                    if(attackTime < coolDownTime){
                        attackTime += Time.deltaTime;
                    }else if(attackTime > coolDownTime){
                        attackTime = 0.0f;
                        if(!attack)StartCoroutine(AttackPlayer());
                    }
                }else{
                    myTransform.LookAt(target);
                    myTransform.position += myTransform.forward * 20 * Time.deltaTime;
                }
            }else{
                GameObject[] imps = GameObject.FindGameObjectsWithTag("Enemy");
                foreach(GameObject go in imps){
                    if(go.name == "Imp"){
                        if(go.GetComponent<ImpAI>().isAggro){
                            isAggro = true;
                            attackState = AttackState.Run;
                        }
                    }
                }
            }
        }else{
            if(!died)StartCoroutine(Death());
            if(dissolve){
                animation.Stop();
                float shininess = Mathf.PingPong(Time.time, 1.0F);
                
                //GameObject.Find("ChargerModel").renderer.material.SetFloat("_SliceAmount",  Mathf.Sin(Time.time) * 1.0f);
            }
        }
        
	}

    
    void AnimaionManager(){
        switch(attackState){
            case AttackState.Idle:
                animation.clip = animation.GetClip("idle");
                animation.CrossFade(animation.clip.name, 0.2f);
                break;
            case AttackState.Walk:
                animation.clip = animation.GetClip("walk");
                animation[animation.clip.name].speed = 1.5f;
                animation.CrossFade(animation.clip.name, 0.2f);
                break;
            case AttackState.Run:
                animation.clip = animation.GetClip("run");
                animation[animation.clip.name].speed = 2f;
                animation.CrossFade(animation.clip.name, 0.2f);
                break;
            case AttackState.Attack:
                animation.clip = animation.GetClip("attack");
                animation.CrossFade(animation.clip.name, 0.2f);
                break;
            case AttackState.Spell:
                animation.clip = animation.GetClip("spell");
                animation.CrossFade(animation.clip.name, 0.2f);
                break;
            case AttackState.Death:
                animation.clip = animation.GetClip("death");
                animation.Play();
                //animation[animation.clip.name].speed = 2.0f;
                //animation.CrossFade(animation.clip.name, 0.2f);
                break;
        }
    }

    IEnumerator AttackPlayer(){
        attack = true;
        attackState = AttackState.Attack;
        Entity entity = (Entity)target.GetComponent(typeof(Entity));
	    entity.AdjustHealth(strength);
        yield return new WaitForSeconds(animation.clip.length);
        attackState = AttackState.Run;
        attack = false;
    }

    IEnumerator Death(){
        died = true;
        attackState = AttackState.Death;
        print(animation.clip.length);
        yield return new WaitForSeconds(animation.clip.length);
        DropLoot();
        dissolve = true;
        yield return new WaitForSeconds(0.7f);
        Destroy(gameObject);
    }

    float CalculateJumpVerticalSpeed(){
	    return Mathf.Sqrt(2 * jumpHeight * gravity);
	}
}
using UnityEngine;
using System.Collections;

public class ImpLeaderAI : Enemy{

    public int jumpHeight = 3;
    public int sideJumpDist = 3;

    public enum AttackState{
        Idle,
        Walk,
        Run,
        Attack,
        Spell,
        Death
    }

    public AttackState attackState = AttackState.Run;

	void Start(){
        InitEntity("Imp", player, 100, 100, 8, 0, 10, 20, 1.5f, 10, 1);
    }
	
	void FixedUpdate(){
        Attack();
        AnimaionManager();
        if(!isAttacking){
            //Move();
        }
        //myTransform.LookAt(target);
	}

    void AnimaionManager(){
        switch(attackState){
            case AttackState.Idle:
                animation.clip = animation.GetClip("idle");
                animation.CrossFade(animation.clip.name, 0.2f);
                break;
            case AttackState.Walk:
                animation.clip = animation.GetClip("walk");
                animation[animation.clip.name].speed = 1.5f;
                animation.CrossFade(animation.clip.name, 0.2f);
                break;
            case AttackState.Run:
                animation.clip = animation.GetClip("run");
                animation[animation.clip.name].speed = 2f;
                animation.CrossFade(animation.clip.name, 0.2f);
                break;
            case AttackState.Attack:
                animation.clip = animation.GetClip("attack");
                animation.CrossFade(animation.clip.name, 0.2f);
                break;
            case AttackState.Spell:
                animation.clip = animation.GetClip("spell");
                animation.CrossFade(animation.clip.name, 0.2f);
                break;
            case AttackState.Death:
                animation.clip = animation.GetClip("death");
                animation.Play();
                //animation[animation.clip.name].speed = 2.0f;
                //animation.CrossFade(animation.clip.name, 0.2f);
                break;
        }
    }

    float CalculateJumpVerticalSpeed(){
	    return Mathf.Sqrt(2 * jumpHeight * gravity);
	}
}
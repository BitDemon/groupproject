using UnityEngine; 
using System.Collections; 
 using System.Collections.Generic;
public class StoreGUI : MonoBehaviour { 

    private Transform myTransform;              // NPC Transform
    private Transform playerTransform;          // Player Transform
    private float playerDistance;               // distance between Player and NPC
 
    public bool displayStore = false;            // show the store
    
	public PlayerControls playercontrols;
	//public int Goldt;
    // define and create our GUI delegate
    private delegate void GUIMethod(); 
    private GUIMethod currentGUIMethod; 
    private Vector2 scrollPosition = Vector2.zero;
    
    // Array with the Itens
    public ArrayList itensArray = new ArrayList();
    public ArrayList itensPriceArray = new ArrayList();
	
 	public List<Items> selectedItemName = new List<Items>();
   
    
    // Array with the Weapons
    public ArrayList weaponsArray = new ArrayList();
    public ArrayList weaponsPriceArray = new ArrayList();
    
    private int amount = 1;                     //amount of itens to buy
    private int totalPrice;                     //total price of itens to buy
    private string selectedItem = "";           // selected item from the store
    private int selectedItemPrice;              // price of the selected item from the store
    
    private Color lerpedColor;                  //just a test

 
    // Use this for initialization
    void Start () { 
            
        GameObject go = GameObject.FindGameObjectWithTag("Player"); //find the GameObject w/ "Player" TAG
		playercontrols = GameObject.Find("Player").GetComponent<PlayerControls>();
        playerTransform = go.transform;                             //and stores its Transform
    
        myTransform = transform;                // NPC transform;
                
        this.currentGUIMethod = itensMenu;      // start with the main menu GUI
 
        selectedItem = "";                      //sets the selected item to null
        
        addStuffToArrays();                     // fills the arrays with the Items
    }
 
    //Update is called Once per frame
    void Update(){
        
        //this is just a test
        lerpedColor = Color.Lerp(Color.green, Color.red, Time.time / 10);
        
        // stores the Distancie between the NPC and the player
        playerDistance = Vector3.Distance(myTransform.position, playerTransform.position);
        
        // if the playerDistance is bigger than 4, close the store)
<<<<<<< HEAD
        if(playerDistance > 40){         
=======
        if(playerDistance > 20){         
>>>>>>> 0b768a5417c445ab7c2feb2b77491c6c2637549c
            displayStore = false;
       }
    }
    
    //Displays the store
    public void OnMouseUpAsButton() {     // When the NPC is clicked...      
		
		 if(playerDistance<20){               // and if playerDistance is lesser than 4...
            displayStore = true;            // then Displays the StoreGUI. 
        }
    }
 

    // Update is called once per frame 
    void OnGUI () {
    
        //Displays the store
        if(displayStore){
        
            //Text Item
            GUI.Label (new Rect (20, 170 , 230, 30), "Item");
            //Text Price
            GUI.Label (new Rect (250, 170, 40, 30), "Price");
        
            //button Exit
            if(GUI.Button (new Rect(220, 130, 100, 30), "Exit Store")){
                displayStore = false;
            }
    
            this.currentGUIMethod(); 
            
            // Area with selected item, amount, total price...
            GUILayout.BeginArea(new Rect( 20, 400, 300, 100));
            
            GUILayout.BeginVertical("box");
            
            GUILayout.BeginHorizontal("Label");
            //Text Item
            GUILayout.Box  (selectedItem.ToString(), GUILayout.Width(110));
 
            // Box with Amount
            GUILayout.Box  (" X " + amount.ToString(), GUILayout.Width(50));
<<<<<<< HEAD
			
			 //Button Amount plus
=======
            
            //Button Amount plus
>>>>>>> 0b768a5417c445ab7c2feb2b77491c6c2637549c
            if(GUILayout.Button (" + ")){
                amount++;
            }
            //button Amount minus
            if(GUILayout.Button (" - ")){
                amount--;
            }
			
            GUILayout.EndHorizontal();            
            
            GUILayout.BeginHorizontal("box");         
                        
           
            //box Total Price
            GUI.color = lerpedColor;
            GUILayout.Box (totalPrice.ToString(), GUILayout.Width(80));
            
            //button Buy
            if(GUILayout.Button ("Buy")){
                addItemToInventory();
            }
            
            GUILayout.EndHorizontal();
            GUILayout.EndVertical();
            GUILayout.EndArea();
            
        }   //ends the displayStore IF
<<<<<<< HEAD
		 
=======
		 GUI.Label(new Rect (20, 5, 100, 30), "Gold " +InventorySystem.Gold);
>>>>>>> 0b768a5417c445ab7c2feb2b77491c6c2637549c
    }

    
   

    public void itensMenu() { 
        
        GUI.Box (new Rect (20, 130, 100, 30), "Itens Menu");  // Box with the Menu Title
        
        //Scroll Area with the Itens displayed Inside                                           // The scroll is dinamic generated by the size of the array
        scrollPosition = GUI.BeginScrollView(new Rect(20, 190, 300, 200), scrollPosition, new Rect(0, 0, 120, itensArray.Count * 30));
            displayItens();
        GUI.EndScrollView();
        
<<<<<<< HEAD
=======
        
>>>>>>> 0b768a5417c445ab7c2feb2b77491c6c2637549c
        
        
        if (GUI.Button (new Rect (120, 130, 100, 30), "Weapons Menu")) {
            switchMenuForWeaponsButton();                //Switch for Weapons Menu
        } 
        
    } 

 

    
    
    

    private void weaponsMenu(){
 
        GUI.Box (new Rect (20, 130, 100, 30), "Weapons Menu");             // Box with the MenuTitle
        
        //Scroll Area with the Itens displayed Inside                                           // The scroll is dinamic generated by the size of the array
        scrollPosition = GUI.BeginScrollView(new Rect(20, 190, 300, 200), scrollPosition, new Rect(0, 0, 120, weaponsArray.Count * 30));
            displayWeapon();
        GUI.EndScrollView();
        
        if (GUI.Button (new Rect (20 + 100, 130, 100, 30), "Itens Menu")) {       
            switchMenuForItensButton();                 // Switch for Itens Menu
        } 
        
 
        
    }    

    void switchMenuForItensButton(){
        this.currentGUIMethod = itensMenu;              // switch to Itens Menu
        amount = 1;                                     // resets the amount of itens selected
    }
 
    
 
    void switchMenuForWeaponsButton(){
        this.currentGUIMethod = weaponsMenu;             // switch to Weapons Menu
        amount = 1;                                     // resets the amount of itens selected
    }

        public void displayItens() {
                            
            // Loop to display all the itensArray of the itensArray
            for(int cnt = 0; cnt < itensArray.Count; cnt++){
            
                //Buttom with Item Name
                if(GUI.Button (new Rect (0, 30 * (cnt), 230, 30), itensArray[cnt].ToString(), "box")){
                    
                    // Selects the item and it´s price
                    selectedItem = itensArray[cnt].ToString();
                    selectedItemPrice = (int)(itensPriceArray[cnt]);
                    
                    // resets the amount of itens selected
                    amount = 1;
                }
            
                // Displays the Item Price
                GUI.Box (new Rect (230, 30 * (cnt), 50, 30), itensPriceArray[cnt].ToString());
                
                // calculates the total price
                totalPrice = amount * (int)(selectedItemPrice);
            } 
        }
	
        public void displayWeapon() {
            
            // Loop to display all the itensArray of the array
            for(int cnt = 0; cnt < weaponsArray.Count; cnt++){
 
                //Buttom with Armor Name
                if(GUI.Button (new Rect (0, 30 * (cnt), 230, 30), weaponsArray[cnt].ToString(), "box")){
                    
                    // Selects the armor and it´s price
                    selectedItem = weaponsArray[cnt].ToString();
                    selectedItemPrice = (int)(weaponsPriceArray[cnt]);
                    
                    // resets the amount of itens selected
                    amount = 1;
                }
                
                // Displays the Item Price
                GUI.Box (new Rect (230, 30 * (cnt), 50, 30), weaponsPriceArray[cnt].ToString());
                
                // calculates the total price
                totalPrice = amount * (int)(selectedItemPrice);
            } 
        }

    public void addItemToInventory(){           // Method for the buy buttom
        
        if(selectedItem != "" ){
			
		if (InventorySystem.Gold >= totalPrice){                 //se selectedItem for diferente de null
            print("Bought Item!");
			InventorySystem.Gold -= totalPrice;
				
			for(int cnt = 0; cnt < amount; cnt++){
			
			selectedItemName.Add(new Items());
						
			selectedItemName[0].Name = selectedItem;
			selectedItemName[0].Cost = (selectedItemPrice/2);
						
									
                InventorySystem.Inventory.Add(selectedItemName[0]);
<<<<<<< HEAD
			}
            
            
=======
				
            }
            
            
				
				
				
				
>>>>>>> 0b768a5417c445ab7c2feb2b77491c6c2637549c
		}
		else {	
			print("Not enough Gold");
		}
			
            // To Add Item X amount; - Use a loop   
            /* 
            for(int cnt = 0; cnt < amount; cnt++){
                addItemToInventory(selectedItem);
            }
            
            curGold - totalPrice;
            */
        } }
    

    // Fills itensArrays, armorsArray, weaponsArray, and PricesArray with the itens
    private void addStuffToArrays(){
    
    // add itens to the itensArray
        itensArray.Add("Health Potion");
        itensArray.Add("Mana Potion");       
            
    // add itens to the itensPriceArray     
        itensPriceArray.Add(25);
<<<<<<< HEAD
        itensPriceArray.Add(25);    
=======
        itensPriceArray.Add(25);
        itensPriceArray.Add(50);
        itensPriceArray.Add(50);
        
    
>>>>>>> 0b768a5417c445ab7c2feb2b77491c6c2637549c
        
        
    // add itens to the weaponsArray
        weaponsArray.Add("Wooden Staff");
        weaponsArray.Add("Giant Staff");
        weaponsArray.Add("Super Ultra Staff");  
        
    // add prices to weaponsPriceArray
        weaponsPriceArray.Add(100);
        weaponsPriceArray.Add(500);
        weaponsPriceArray.Add(1000);
    }

}
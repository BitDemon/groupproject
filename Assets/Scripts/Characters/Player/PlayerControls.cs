using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerControls : Entity{

    private Quaternion OriginalRotation;
    private Camera mCamera;
    public bool walking, running, runAuto, jumping, runJump, autoAttack, stunned;
	
	public AudioSource grunt;
	public AudioSource fireball;
<<<<<<< HEAD

    public Transform playerHead;
=======
>>>>>>> 0b768a5417c445ab7c2feb2b77491c6c2637549c

    public enum PlayerState{
        Idle,
        Walk,
        Run,
        Auto,
        Fireball,
        Snare,
        Stunned,
        Jump,
        Death,
        RunAuto,
        RunSpell
    };

    public enum AttackState{
        None,
        Attacking
    };

    //Player Controls
    public PlayerState playerState = PlayerState.Idle;
    public AttackState attackState = AttackState.None;
    public int runSpeed = 13;
    public float dodgeAmount = 300f;

    public float maxMana = 100;
    public float mana = 100;
    public float jumpHeight = 13.0f;

    //Camera Controls
    public float RotationX = 0f;
    public float RotationY = 0f;
    public float prevRotationX = 0f;
    public float prevRotationY = 0f;

    public float sensitivityX = 200f;
    public float sensitivityY = 20f;
    public float minimumY = 0f;
    public float maximumY = 10f;

    public float zoomSpeed = 20;

    //Spells
    public List<GameObject> SpellPrefabs;
    public float delayBetweenFiring = 1.3f;
    public float delayToFire;

	void Start(){
        InitEntity("Player", null, 100, 100, 9, 2, 30, 60, 5f, 0, 1);

        OriginalRotation = transform.localRotation;
        mCamera = Camera.mainCamera;
        mCamera.transform.LookAt(myTransform);

        coolDownTime = animation.GetClip("auto").length;
        attackTime = coolDownTime;
		
		grunt=(AudioSource)gameObject.AddComponent("AudioSource");
		AudioClip myAudioClip;
		myAudioClip =(AudioClip)Resources.Load("Sounds/grunt");
		grunt.clip = myAudioClip;
		
		fireball=(AudioSource)gameObject.AddComponent("AudioSource");
		AudioClip myAudioClip1;
		myAudioClip1=(AudioClip)Resources.Load("Sounds/FireBall");
		fireball.clip = myAudioClip1;
	}

	void Update(){
        InputHandeling();
        AnimaionManager();
<<<<<<< HEAD
        if(!stunned)
            AttackEnemy();
=======
        AttackEnemy();
>>>>>>> 0b768a5417c445ab7c2feb2b77491c6c2637549c
	}

    void InputHandeling(){
        //Key Input

<<<<<<< HEAD
        if (Input.GetKeyDown(KeyCode.I)){
            Messenger.Broadcast("OpenCloseInventory");
=======
        if (Input.GetKeyDown(KeyCode.I))
        {
            Messenger.Broadcast("OpenCloseInventory");
        }
        else if(Input.GetKeyDown(KeyCode.Q)){
            rigidbody.AddForce(Vector3.left * dodgeAmount * Time.deltaTime, ForceMode.Impulse);
        }else if(Input.GetKeyDown(KeyCode.E)){
            rigidbody.AddForce(Vector3.right * dodgeAmount * Time.deltaTime, ForceMode.Impulse);
>>>>>>> 0b768a5417c445ab7c2feb2b77491c6c2637549c
        }

        if(Input.GetKeyDown(KeyCode.LeftShift)){
            if(walking){
                walking = false;
                running = true;
            }else{
                walking = true;
                running = false;
            }
        }

        //if(attackState != AttackState.Attacking){
<<<<<<< HEAD
        if(!stunned){
            float inputX = Input.GetAxis("Horizontal");
            float inputY = Input.GetAxis("Vertical");

            RotationX = Input.GetAxis("Mouse X") * sensitivityX * Time.deltaTime;
            RotationY -= Input.GetAxis("Mouse Y") * sensitivityY * Time.deltaTime;
            RotationY = Mathf.Clamp(RotationY, minimumY, maximumY);
=======
        if(playerState != PlayerState.Stunned){
            float inputX = Input.GetAxis("Horizontal");
            float inputY = Input.GetAxis("Vertical");
>>>>>>> 0b768a5417c445ab7c2feb2b77491c6c2637549c
            
            Vector3 targetVelocity = new Vector3(0, 0, inputY);
            targetVelocity = transform.TransformDirection(targetVelocity);
            
            if(walking){
                targetVelocity *= moveSpeed;
            }else{
                targetVelocity *= runSpeed;
            }

            myTransform.position += targetVelocity * Time.deltaTime;

            
            if(Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.S)){
                if(walking){
                    playerState = PlayerState.Walk;
                }else{
                    if(!runAuto && !runJump)
                        playerState = PlayerState.Run;
                }
            }else if(Input.GetKeyUp(KeyCode.W) || Input.GetKeyUp(KeyCode.S)){
                playerState = PlayerState.Idle;
            }

            Vector3 rotation = new Vector3(0, Input.GetAxis("Horizontal"), 0);
            rotation *= rotationSpeed;
            myTransform.Rotate(rotation);

<<<<<<< HEAD
            minimumY = playerHead.position.y - 15;
            maximumY = playerHead.position.y + 50;
=======
            minimumY = myTransform.position.y + 4;
            maximumY = myTransform.position.y + 20;
>>>>>>> 0b768a5417c445ab7c2feb2b77491c6c2637549c
            
            RotationX = Input.GetAxis("Mouse X") * sensitivityX * Time.deltaTime;
            RotationY -= Input.GetAxis("Mouse Y") * sensitivityY * Time.deltaTime;
            RotationY = Mathf.Clamp(RotationY, minimumY, maximumY);
<<<<<<< HEAD
            
            if(Input.GetMouseButton(1)){
                mCamera.transform.position = new Vector3(mCamera.transform.position.x, RotationY, mCamera.transform.position.z);
                mCamera.transform.RotateAround(myTransform.position, Vector3.up, RotationX);
            }

            mCamera.transform.LookAt(playerHead);

=======

            if(Input.GetMouseButton(1)){
                mCamera.transform.position = new Vector3(mCamera.transform.position.x, RotationY, mCamera.transform.position.z);
                mCamera.transform.RotateAround(myTransform.position, Vector3.up, RotationX);
                mCamera.transform.LookAt(myTransform);
            }

>>>>>>> 0b768a5417c445ab7c2feb2b77491c6c2637549c
            if(isGrounded){
                StartCoroutine(Jump());
            }

            if(Input.GetMouseButton(0)){
                if(!autoAttack)
                    StartCoroutine(AutoAttack());
            }
        }
        //}

        if(Input.GetKeyDown(KeyCode.Alpha1)){
            selectedSpell = spells[0];
<<<<<<< HEAD
            StartCoroutine(CastSpell());
        }else if(Input.GetKeyDown(KeyCode.Alpha2)){
            selectedSpell = spells[1];
            StartCoroutine(CastSpell());
        }

=======
        }else if(Input.GetKeyDown(KeyCode.Alpha2)){
            selectedSpell = spells[1];
        }

        
>>>>>>> 0b768a5417c445ab7c2feb2b77491c6c2637549c
        Vector3 down = transform.TransformDirection(Vector3.down);
        if(Physics.Raycast(transform.position, down, 0.5f)){
            isGrounded = true;
            jumping = false;
        }else{
            isGrounded = false;
            jumping = false;
        }
    }

    void AnimaionManager(){
        switch(playerState){
            case PlayerState.Idle:
                animation.clip = animation.GetClip("idle");
                animation.CrossFade(animation.clip.name, 0.5f);
                break;
            case PlayerState.Walk:
                animation.clip = animation.GetClip("walk");
                animation.CrossFade(animation.clip.name, 0.5f);
                break;
            case PlayerState.Run:
                animation.clip = animation.GetClip("run");
                animation.CrossFade(animation.clip.name, 0.2f);
                break;
            case PlayerState.Auto:
                animation.clip = animation.GetClip("auto");
                animation.CrossFade(animation.clip.name, 0.2f);
<<<<<<< HEAD
				grunt.Play();
=======
				grunt.Play ();
>>>>>>> 0b768a5417c445ab7c2feb2b77491c6c2637549c
                break;
            case PlayerState.Fireball:
                animation.clip = animation.GetClip("fireball");
                animation.CrossFade(animation.clip.name, 0.2f);
                break;
            case PlayerState.Snare:
                animation.clip = animation.GetClip("snare");
                animation.CrossFade(animation.clip.name, 0.2f);
                break;
            case PlayerState.Stunned:
                animation.clip = animation.GetClip("stunned");
                animation.CrossFade(animation.clip.name, 0.2f);
                break;
            case PlayerState.Jump:
                animation.clip = animation.GetClip("jump");
                animation.CrossFade(animation.clip.name, 0.2f);
                break;
            case PlayerState.Death:
                animation.clip = animation.GetClip("death");
                animation.CrossFade(animation.clip.name, 0.2f);
                break;
            case PlayerState.RunAuto:
                animation.clip = animation.GetClip("runauto");
                animation.CrossFade(animation.clip.name, 0.2f);
                break;
            case PlayerState.RunSpell:
                animation.clip = animation.GetClip("runspell");
                animation.CrossFade(animation.clip.name, 0.2f);
                break;
        }
    }

    IEnumerator Jump(){
        if(Input.GetKeyDown(KeyCode.Space) && !jumping){
            jumping = true;
            if(playerState == PlayerState.Run){
                runJump = true;
            }else{
                runJump = false;
            }
            playerState = PlayerState.Jump;
            animation["jump"].speed = 2.0f;
            yield return new WaitForSeconds(0.3f);
            Vector3 velocity = rigidbody.velocity;
            rigidbody.velocity = new Vector3(velocity.x, CalculateJumpVerticalSpeed(), velocity.z);
            yield return new WaitForSeconds(0.05f);
            runJump = false;
            playerState = PlayerState.Idle;
            animation.CrossFade(animation.clip.name, 0.06f);
            
        }
    }

    public IEnumerator Stunned(){
        playerState = PlayerState.Stunned;
        stunned = true;
        yield return new WaitForSeconds(4);
        playerState = PlayerState.Idle;
        stunned = false;
    }

    public void AttackEnemy(){
        if(health == 0){
            //mCamera.transform.parent = null;
            //target = null;
            //player = null;
            //Destroy(gameObject);
        }

        //StartCoroutine(CastSpell());

        isAttacking = false;
        if(FindClosestEnemy())playerEnemy = FindClosestEnemy().transform;
        if(playerEnemy){
<<<<<<< HEAD
            var relativePoint = myTransform.InverseTransformPoint(playerEnemy.position);
=======
            var relativePoint = transform.InverseTransformPoint(playerEnemy.position);
>>>>>>> 0b768a5417c445ab7c2feb2b77491c6c2637549c
            if(relativePoint.x > -4.0f && relativePoint.x < 4.0 && relativePoint.z < 20){
                isAttacking = true;
            }else{
                isAttacking = false;
                playerEnemy = null;
            }
        }
    }

    IEnumerator AutoAttack(){
        autoAttack = true;
        attackState = AttackState.Attacking;
        if(Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.S)){
            runAuto = true;
            playerState = PlayerState.RunAuto;
        }else{
            playerState = PlayerState.Auto;
        }
        autoAttack = true;
        yield return new WaitForSeconds(animation.clip.length / 2);
        if(isAttacking){
            Enemy enemy = (Enemy)playerEnemy.GetComponent(typeof(Enemy));
            enemy.AdjustHealth(strength);
        }
        
        yield return new WaitForSeconds(0.1f);
        attackState = AttackState.None;
        playerState = PlayerState.Idle;
        runAuto = false;
        autoAttack = false;
    }

    public void AdjustMana(int adj){
		mana -= adj;
		
		if(mana<0){
			mana=0;
		} 
		if (mana > maxMana){
		    mana = maxMana;
		}
	}

    private float timeDelay = 0.0f;

    public Rigidbody fireballObject;
    public Transform spawnPos;
    public float damage = 10f;
    bool castingSpell = false;

    public List<Transform> spells;
    public Transform selectedSpell;

    public IEnumerator CastSpell(){
<<<<<<< HEAD
        if(!castingSpell){
=======
        if(Input.GetMouseButtonDown(1) && !castingSpell){
>>>>>>> 0b768a5417c445ab7c2feb2b77491c6c2637549c
            attackState = AttackState.Attacking;
            switch(spells.IndexOf(selectedSpell)){
                case 0:
                    playerState = PlayerState.Fireball;
                    break;
                case 1:
                    playerState = PlayerState.Snare;
                    break;
            }
            castingSpell = true;
            yield return new WaitForSeconds(animation.clip.length/2);
            switch(spells.IndexOf(selectedSpell)){
                case 0:
                    ShootFireball();
<<<<<<< HEAD
                    AdjustMana(10);
                    break;
                case 1:
                    PlaceAOE();
                    AdjustMana(18);
=======
                    break;
                case 1:
                    PlaceAOE();
>>>>>>> 0b768a5417c445ab7c2feb2b77491c6c2637549c
                    break;
            }
            yield return new WaitForSeconds((0.1f));
            playerState = PlayerState.Idle;
            attackState = AttackState.None;
            castingSpell = false;
        }
    }

    void ShootFireball(){
        Rigidbody newFireballObject = Instantiate(selectedSpell.rigidbody, spawnPos.position, spawnPos.rotation) as Rigidbody;
        newFireballObject.name = "fireball";
        newFireballObject.AddForce(transform.forward * 4000.0f);
        Destroy(newFireballObject.gameObject, 2.5f);
		fireball.Play();
    }

    bool place = false;
    void PlaceAOE(){
<<<<<<< HEAD
        Vector3 newPos = playerHead.position + (myTransform.forward * 10) + (myTransform.up * 31);
=======
        Vector3 newPos = myTransform.position + (myTransform.forward * 10);
>>>>>>> 0b768a5417c445ab7c2feb2b77491c6c2637549c
        GameObject aoe = Instantiate(selectedSpell.gameObject, newPos, Quaternion.identity) as GameObject;
    }

    void OnGUI(){
        GUI.Label(new Rect(100, 5, 200, 200), "Spell: " + selectedSpell.name);
    }

<<<<<<< HEAD
    float CalculateJumpVerticalSpeed(){
=======
    float CalculateJumpVerticalSpeed () {
>>>>>>> 0b768a5417c445ab7c2feb2b77491c6c2637549c
	    return Mathf.Sqrt(2 * jumpHeight * gravity);
	}

    GameObject FindClosestEnemy(){ 
        // Find all game objects with tag Enemy 
        GameObject[] gos;
        gos = GameObject.FindGameObjectsWithTag("Enemy");
        if(gos == null)return null;
        GameObject closest = null; 
        var distance = Mathf.Infinity; 
        var position = transform.position; 
        // Iterate through them and find the closest one 
        foreach (GameObject go in gos){ 
           var diff = (go.transform.position - position); 
           var curDistance = diff.sqrMagnitude; 
           if(curDistance < distance){ 
             closest = go;
             distance = curDistance; 
           } 
        } 
        return closest; 
    }
}
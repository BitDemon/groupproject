using UnityEngine;
using System.Collections;

public class CastingSpells : MonoBehaviour{
    PlayerControls playerControls;

    private float timeDelay = 0.0f;

    public Rigidbody fireballObject;
    public Transform spawnPos;
    public float delayAmount = 2.0f;
    public float damage = 10f;
    
    void Awake(){
        playerControls = GetComponent<PlayerControls>();
    }

	void Update(){
        if(Input.GetMouseButtonDown(0)){ShootSpell();}
	}

    public IEnumerator ShootSpell(){
        if(Input.GetMouseButtonDown(0)){
            playerControls.playerState = PlayerControls.PlayerState.Fireball;
            playerControls.attackState = PlayerControls.AttackState.Attacking;
            Rigidbody newFireballObject = Instantiate(fireballObject, spawnPos.position, spawnPos.rotation) as Rigidbody;
            newFireballObject.name = "fireball";
            newFireballObject.AddForce(transform.forward * 4000.0f);
            Destroy(newFireballObject.gameObject, 2.5f);
            yield return new WaitForSeconds(animation.clip.length);
            playerControls.playerState = PlayerControls.PlayerState.Idle;
            playerControls.attackState = PlayerControls.AttackState.None;
        }
    }
}

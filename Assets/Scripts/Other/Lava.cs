using UnityEngine;
using System.Collections;

public class Lava : MonoBehaviour {

    public float scrollSpeed = 0.0F;
    float randomOffset;
	public PlayerControls playercontrols;

	void Awake(){
		playercontrols = GameObject.Find("Player").GetComponent<PlayerControls>();
        scrollSpeed = Random.Range(0.1f, 0.3f);
        randomOffset = Random.Range(0.0f, 10.0f);
        renderer.material.SetTextureOffset("_MainTex", new Vector2(randomOffset, 0));
        renderer.material.SetTextureOffset("_BumpMap", new Vector2(randomOffset, 0));
	}
	
	void Update(){
	    float offset = (randomOffset + Time.time) * scrollSpeed;
        renderer.material.SetTextureOffset("_MainTex", new Vector2(offset, 0));
        renderer.material.SetTextureOffset("_BumpMap", new Vector2(offset, 0));
		
	}
	
	void OnTriggerEnter (Collider collider){
		if (collider.transform.tag == "Player"){
			Destroy(GameObject.FindWithTag("Player"));
		}
}
}

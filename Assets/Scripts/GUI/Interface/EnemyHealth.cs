using UnityEngine;
using System.Collections;

public class EnemyHealth : MonoBehaviour{
	PlayerControls player;
	Enemy enemyTarget;
    RaycastHit hit;

	public Transform target; 

	public GUITexture background = null;
	public GUITexture healthbar = null;

    public int enemyHealth;
	
	// Use this for initialization
	void Init(){
		player = GameObject.Find("Player").GetComponent<PlayerControls>();
	}
	
	void Awake(){
		Init();	
	}

	void Update(){
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if(Physics.Raycast(ray, out hit)){
            if(hit.transform.tag == "Enemy"){
                target = hit.transform;
                enemyTarget = (Enemy)target.GetComponent(typeof(Enemy));
                enemyHealth = enemyTarget.health;
			    background.enabled = true;
                healthbar.enabled = true;
		  	    Vector3 wantedPos = Camera.main.WorldToViewportPoint(target.position);
	    	    background.transform.position = wantedPos;
                healthbar.transform.position = wantedPos + new Vector3(0, 0, 0.1f);
			    healthbar.pixelInset = new Rect(0, 0, enemyHealth, healthbar.pixelInset.height);
            }else{
                background.enabled = false;
                healthbar.enabled = false;
            }
        }
	}
}